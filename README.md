# Doksi static page

This is the source code for [doksi.gitlab.io](https://doksi.gitlab.io).

## Build

Check out the repository and run the following to build the page:

```
gem install bundler
bundle install
bundle exec jekyll build -d public
```

The page is now in the folder `./public`.
