---
layout: post
title:  "Doksi 0.2.0 has been released"
date:   2020-07-03 19:57:29 +0200
categories: releases
nav: blog
image: assets/images/blog/posters/release-0.2.0.png
---
It's the second coming. We are back just one month since the last release with a slew of new features. We promised new block types, we promised more meta data, we promised "soon", we delivered!

### New block type: Quote

The first new block type is a Quote. This block has a content (the quote itself) and a source (whoever the quote comes from). They are displayed with a little quotation mark to make it stand out from other paragraphs.

### New block type: Code

The second type of block we've added is a code block. You can now paste in your code snippets and have them be highlighted for easier reading. We are currently using [CodeRay](http://coderay.rubychan.de/) but will probably move over to Pygments later since that's what is used in Sphinx when generating documents from ReST files.

### Paragraph types

Lastly, while not really a new type of block, we have made it possible to change Paragraphs from just plain text to be tips, warnings or notes. When marking a paragraph as one of these kinds they will be displayed with a little icon and a heading, making them stand out from other paragraphs.

Use this to highlight tips or warnings to readers.

### Classification

Last time we also promised to deliver more kind of meta data for documents and the first one after copyright notice is classification. By default we ship NATO classifications but you can create your own as well. This way documents can be marked as Secret, Confidential or Open.

Later on we plan on tying this to forced encryption, watermarks and stamps.

### Broken stuff that ain't no more

- It was not possible to sign up with an provider using the same email as an existing user.
